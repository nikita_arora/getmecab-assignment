package swipetabs.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.nikitaarora.getmecab.TabFragment1;
import com.example.nikitaarora.getmecab.TabFragment2;

/**
 * Created by nikita.arora on 12/29/2015.
 */
public class TabsPagerAdapter extends FragmentStatePagerAdapter {
    int mTabs;

    public TabsPagerAdapter(FragmentManager fm, int mTabs) {
        super(fm);
        this.mTabs = mTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                TabFragment1 tab1 = new TabFragment1();
                return tab1;

            case 1:
                TabFragment2 tab2 = new TabFragment2();
                return tab2;

            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return mTabs;
    }
}
